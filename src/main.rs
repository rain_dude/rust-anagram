mod filtering;
mod glue_words;
mod letters;
mod strings;
mod words_group;

use filtering::filter_words;
use multimap::MultiMap;
use std::{collections::HashSet, env, fs, ops::Not, path::Path, time::Instant};

use crate::{glue_words::Gluer, letters::Sorted};

fn main() {
    let execution_start_time = Instant::now();

    let input_args: Vec<String> = env::args().collect();
    let input_args = prepare_input_words(input_args);
    println!("STARTED with input: {:?}", input_args);

    let name: String = input_args
        .iter()
        .fold(String::new(), |prev, next| format!("{}{}", prev, next));
    let name_letters = Sorted::from_str(&name);

    let dict_words_dir_path = "/usr/share/dict/";
    let polish_dict_words = "polish";
    let american_english_dict_words = "american-english";
    let selected_dict_lang = american_english_dict_words;
    let selected_words_path = dict_words_dir_path.to_string() + selected_dict_lang;
    let words_of_letters_path = merge_all(vec![
        "/tmp/words_".to_string(),
        selected_dict_lang.to_string(),
        "_".to_string(),
        name_letters.to_string(),
    ]);
    println!("Words file path: {}", words_of_letters_path);

    let words_not_calculated = Path::new(&words_of_letters_path).exists().not();
    println!("Words available? {}", !words_not_calculated);
    let words: Vec<String> = if words_not_calculated {
        calculate_words(&selected_words_path, &words_of_letters_path, &name_letters)
    } else {
        load_words_of_letters(&words_of_letters_path)
            .into_iter()
            .map(|word| word.to_string())
            .collect()
    };
    let words: Vec<&str> = words
        .iter()
        .map(|word| {
            let word: &str = word;
            word
        })
        .collect();

    let grouped_anagrams: MultiMap<String, &str> = group_anagrams(&words);
    let mut distincted: Vec<&str> = grouped_anagrams
        .keys()
        .map(|key| {
            let key: &str = key;
            key
        })
        .collect();
    distincted.sort();

    let search_then_glue = Gluer::new_gluer(Sorted::from_str(&name));
    let found_complete_anagrams = search_then_glue.check_word(&distincted);

    let permutations = unfold_permutations(&found_complete_anagrams, &grouped_anagrams);
    println!("Found groups: [{}]", permutations.len());
    permutations
        .iter()
        .for_each(|distinct_permutations| println!("{:?}", distinct_permutations));

    println!("Time elapsed: {:?}", execution_start_time.elapsed());
}

fn prepare_input_words(input_args: Vec<String>) -> HashSet<String> {
    let input_args: HashSet<String> = input_args
        .iter()
        .skip(1)
        .into_iter()
        .map(|arg| arg.to_string())
        .collect();
    let input_args = if input_args.is_empty() {
        vec!["Test".to_string(), "Me".to_string()]
            .into_iter()
            .collect()
    } else {
        input_args
    };
    input_args
}

fn unfold_permutations<'a>(
    found_anagrams: &'a HashSet<words_group::Words>,
    grouped_anagrams: &'a MultiMap<String, &'a str>,
) -> Vec<Vec<&'a Vec<&'a str>>> {
    let mut unfolded: Vec<_> = found_anagrams
        .iter()
        .map(|distinct_anagram| unfold_anagram(distinct_anagram, grouped_anagrams))
        .collect();
    unfolded.sort_by(|prev, next| prev.len().cmp(&next.len()));

    unfolded
}

fn unfold_anagram<'a>(
    distinct_anagram: &'a words_group::Words,
    grouped_anagrams: &'a MultiMap<String, &'a str>,
) -> Vec<&'a Vec<&'a str>> {
    let unfolded_anagram: Vec<_> = distinct_anagram
        .to_owned()
        .group()
        .into_iter()
        .map(|word| grouped_anagrams.get_vec(word))
        .flatten()
        .collect();

    unfolded_anagram
}

fn group_anagrams<'a>(words_of_letters: &Vec<&'a str>) -> MultiMap<String, &'a str> {
    let mut multiwords: MultiMap<String, &str> = MultiMap::new();

    words_of_letters.iter().for_each(|word| {
        let key = Sorted::from_str(word).to_string();
        multiwords.insert(key, word);
    });

    multiwords
}

fn merge_all(all: Vec<String>) -> String {
    let mut merged = String::new();
    all.iter().for_each(|word| merged.push_str(word));

    merged
}

fn load_words_of_letters<'a>(words_of_letters_path: &'a str) -> Vec<String> {
    let file = fs::read_to_string(words_of_letters_path).unwrap();
    let words_of_letter_from_file: Vec<String> = file
        .split_whitespace()
        .into_iter()
        .map(|word| word.to_string())
        .collect();

    words_of_letter_from_file
}

fn calculate_words<'a>(
    selected_words_path: &'a str,
    result_path: &'a str,
    name_letters: &'a Sorted,
) -> Vec<String> {
    let filtered_letters = filter_words(selected_words_path, name_letters);
    save_filtered_words(&filtered_letters, result_path);

    filtered_letters
}

fn save_filtered_words(filtered_letters: &Vec<String>, result_path: &str) {
    let formatted = filtered_letters
        .into_iter()
        .fold(String::new(), |prev, next| format!("{}\n{}", prev, next));
    fs::write(result_path, formatted).unwrap();
}
